using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using UnityEngine;
using UnityEngine.UIElements;

public class CubeTerraria : MonoBehaviour
{   

    public float TimeSpawn;
    

    // Update is called once per frame
    private void Update()
    {
        TimeSpawn += Time.deltaTime;

        if (TimeSpawn >= 5)
        {
            Destroy(gameObject);
        }
    }
}