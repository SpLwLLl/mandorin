using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hallo : MonoBehaviour
{
    public GameObject Box;
    public float time;
    public Transform Point;
    public bool on;
    public float TimeSpawn;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    private void Update()
    {
        TimeSpawn += Time.deltaTime;

        if (TimeSpawn >= 2.5)
        {
            Instantiate(Box, Point.position, Quaternion.identity);
            TimeSpawn = 0;
        }
    }
}