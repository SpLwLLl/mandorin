using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OnOff : MonoBehaviour
{
    public GameObject ArObj;
    private bool on;

    public void Off()
    {
        if (on == true)
        {
            ArObj.SetActive(false);
            on = false;
        }

        else if (on == false)
        {
            ArObj.SetActive(true);
            on = true;
        }
    }
}
