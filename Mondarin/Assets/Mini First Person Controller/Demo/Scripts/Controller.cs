using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject Box;
    public Transform SpawnPoint;
    // Update is called once per frame
    private void Update()
    { 
        if (Input.GetKeyDown(KeyCode.G))
        {
            Instantiate(Box, SpawnPoint.position, Quaternion.identity);
        }
    }
}
