using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour
{ 
public GameObject coin;
public Transform SpawnPoint;

    private void OnTriggerEnter(Collider collision)

    {
        if (collision.gameObject.tag == "Player") 
        {
            Instantiate(coin, SpawnPoint.position, Quaternion.identity);
        }
    }
}





