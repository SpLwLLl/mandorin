using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonOff : MonoBehaviour
{

    public GameObject ArObj;
    private bool on;

    public void off()
    {
        if (on == true)

        { 
                ArObj.SetActive(false);
                on = false;
        }
        else if (on == false)
          { 
                ArObj.SetActive(true);
                on = true;
           }
        
       }
 }
  

