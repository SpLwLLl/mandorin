using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Aq : MonoBehaviour
{
    public Transform Player;
    float distance;
    NavMeshAgent myAgent;
    Animator myAnim;
    private void Start()
    {
        myAgent = GetComponent<NavMeshAgent>();
        myAnim = GetComponent<Animator>();
    }
    private void Update()
    {

        distance = Vector3.Distance(transform.position, Player.position);
        if (distance <= 70)
        {
            myAgent.enabled = true;
            myAgent.SetDestination(Player.position);

        }
        if (distance <= 1.5)
        {
            myAgent.enabled = false;

        }
    }

    
}
