using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class Player1 : MonoBehaviour
{
    public float Point = 0;
    public TMP_Text MondarinText;
    public GameObject Text;

    void Start()
    {
        Text.SetActive(true);
        MondarinText.text = ((int)Point).ToString();
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return)) 
        {
            Text.SetActive(false);
        }

        if (Point >= 7)
        {
            SceneManager.LoadScene("Win");
        }
    }
    void UpdateText()
    {
        MondarinText.text = ((int)Point).ToString();
    }
    public void FixedUpdate()
    {
        UpdateText();
        
    }
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Mondarin")
        {
            Point += 1;
        }
    }
    
}
